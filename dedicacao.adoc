== Dedicação

A _Ludwig von Mises_ (homem, economia e estado)

e aos _libertários do passado_, que iluminaram o caminho, e _libertários do
futuro_, que devem superá-lo (poder e mercado)
