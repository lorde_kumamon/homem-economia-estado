#!/usr/bin/env bash

asciidoctor -b docbook5 book.adoc
pandoc --epub-cover-image=cover.png \
       --epub-metadata=metadata.xml \
       -f docbook book.xml -o book.epub
#epubcheck book.epub
rm book.xml
